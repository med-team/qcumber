# Test Suite

The test suite is made up of two main parts
test_qcumber2.py and a config file called test_run.yaml 

## Development Build of Qcumber2 
    
- cd to target install location and clone

    ```
    $ cd target/location
    $ git clone https://gitlab.com/RKIBioinformaticsPipelines/QCumber.git
    $ cd QCumber
    ``` 

- create new conda environment and activate it

    ```
    $ conda create -f environment/packages.yaml -n qcumber_env
    $ source activate qcumber_env
    ```

- you can generate adapters for adapter removal done by trimmomatic
  by executing the build srcipt 
    
    ```
    $ ./buld.sh 
    ```

## Side Note

- If you are using bash you can do the following 
    
    ```
    $ source completion/_test_qcumber.bash
    ```
    
- It enables tab completion for the test script parameters 

## Basic Testing


Default tests and their corresponding gold standard should be present in
the cloned repository. 

- From the main QCumber directory change to the test directory
  and execute the test script
    
    ```
    $ ./test_qcumber2.py
    ```
    
- All tests should only yield OK or a Warnings, if there is an Error
  you should take a detailed look at the test run. The QCResults folder
  lies within the created testfolder in the test directory.
      
## Advanced Testing and Designing new Tests 

Here tab completion helps to remain sane 
